/***********
 * IMPORTS * 
 ***********/  

import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';


/*********
 * PATHS *
 *********/

const __baseDirPath = path.dirname(fileURLToPath(import.meta.url));
const __deliveriesJSONFilePath = path.join(__baseDirPath, 'data/JSON_data/deliveries.json')
const __matchesJSONFilePath = path.join(__baseDirPath, 'data/JSON_data/matches.json')


/********************
 * Helper Functions *
 ********************/

const putInningsDataIntoObject = (delivery) => {
    return {
        match_id: delivery.match_id,
        inning: delivery.inning,
        bowling_team: delivery.bowling_team,
        batting_team: delivery.batting_team
    };
}

const putOversDataIntoObject = (delivery) => {
    return {
        bowler: delivery.bowler,
        is_super_over: delivery.is_super_over,
        overs: delivery.over
    };
}

const putDeliveriesDataIntoObject = (delivery) => {
    return {
        match_id: delivery.match_id,
        inning: delivery.inning,
        batting_team: delivery.batting_team,
        bowling_team: delivery.bowling_team,
        overs: delivery.over,
        ball: delivery.ball,
        batsman: delivery.batsman,
        non_striker: delivery.non_striker,
        bowler: delivery.bowler,
        is_super_over: delivery.is_super_over,
        wide_runs: delivery.wide_runs,
        bye_runs: delivery.bye_runs,
        legbye_runs: delivery.legbye_runs,
        noball_runs: delivery.noball_runs,
        penalty_runs: delivery.penalty_runs,
        batsman_runs: delivery.batsman_runs,
        extra_runs: delivery.extra_runs,
        total_runs: delivery.total_runs,
        player_dismissed: delivery.player_dismissed,
        dismissal_kind: delivery.dismissal_kind,
        fielder: delivery.fielder
    }
}

const putMatchesDataIntoObject = (match, teamsNameArray, playersNameArray, umpireNameArray, cityVenueObject) => {
    let venue_id = 0
    for (let city in cityVenueObject) {
        if (cityVenueObject[city].includes(match.venue)) {
            venue_id += cityVenueObject[city].indexOf(match.venue) + 1;
            break; 
        } else {
            venue_id += cityVenueObject[city].length; 
        }
    }

    return {
        match_id: match.id,
        venue_id: venue_id,
        season: match.season,
        date: match.date,
        team1: (match.team1 !== '')? (teamsNameArray.indexOf(match.team1)+1): null,
        team2: (match.team2 !== '')? (teamsNameArray.indexOf(match.team2)+1): null,
        toss_winner: (match.toss_winner !== '')? (teamsNameArray.indexOf(match.toss_winner)+1): null,
        toss_decision: match.toss_decision,
        toss_result: match.result,
        winner: (match.winner !== '')? (teamsNameArray.indexOf(match.winner)+1): null,
        dl_applied: match.dl_applied,
        win_by_runs: match.win_by_runs,
        win_by_wickets: match.win_by_wickets,
        player_of_match: (match.player_of_match !== '')? (playersNameArray.indexOf(match.player_of_match) + 1): null,
        umpire1: (match.umpire1 !== '')? (umpireNameArray.indexOf(match.umpire1) + 1): null,
        umpire2: (match.umpire2 !== '')? (umpireNameArray.indexOf(match.umpire2) + 1): null,
        umpire3: (match.umpire3 !== '')? (umpireNameArray.indexOf(match.umpire3) + 1): null          
    }
}



const returnTeamsNameArray = (matchesData) => {
    return matchesData.reduce((teamNameArray, match, i) => {
        if (!teamNameArray.includes(match.team1)) {
            teamNameArray.push(match.team1);
        }
        return teamNameArray;
    }, [])
}

const returnUmpiresNameArray = (matchesData) => {
    return matchesData.reduce((umpireNameArray, match) => {
        if (!umpireNameArray.includes(match.umpire1) && match.umpire1 !== '') {
            umpireNameArray.push(match.umpire1);
        }
        if (!umpireNameArray.includes(match.umpire2) && match.umpire2 !== '') {
            umpireNameArray.push(match.umpire2);
        }
        if (!umpireNameArray.includes(match.umpire3) && match.umpire3 !== '') {
            umpireNameArray.push(match.umpire3);
        }
        return umpireNameArray;
    }, [])
}

const returnPlayersNameArray = (deliveriesData) => {
    return deliveriesData.reduce((playersNameArray, delivery) => {
        if (!playersNameArray.includes(delivery.batsman)) {
            playersNameArray.push(delivery.batsman);
        } 
        if (!playersNameArray.includes(delivery.non_striker)) {
            playersNameArray.push(delivery.non_striker);
        }
        if (!playersNameArray.includes(delivery.bowler)) {
            playersNameArray.push(delivery.bowler);
        }
        return playersNameArray;
    }, [])
}



const returnCityVenueObject = (matchesData) => {
    return matchesData.reduce((cityVenueObject, match) => {
        if (cityVenueObject.hasOwnProperty(match.city) && !cityVenueObject[match.city].includes(match.venue)) {
            cityVenueObject[match.city].push(match.venue);
        } else if (!cityVenueObject.hasOwnProperty(match.city)) {
            cityVenueObject[match.city]  = [match.venue];
        }
        return cityVenueObject;
    }, {})
}

/********************
 * Export Functions *
********************/


// for Creating JSON data for teams table using matches.json file.

const CreateJsonDataForTeams = (matchesData) => {
    /**************************************************************************
     * Getting the teamsNameArray from returnTeamsNameArray() and chaining it *
     * with reduce method to get the array of objects having team_name and    *
     * unique team_id.                                                        *
     **************************************************************************/
    return returnTeamsNameArray(matchesData).reduce((teamsNameObjectArray, teamName, i) => {
        const teamNameObject = {};
        teamNameObject.team_id = i + 1; //team_id is unique id for each team which is (index_value + 1). 
        teamNameObject.team_name = teamName;
        teamsNameObjectArray.push(teamNameObject);
        return teamsNameObjectArray;
    }, [])
}   


// For creating JSON data for players table using matches.json file.

const CreateJsonDataForPlayers = (deliveriesData) => {
    return returnPlayersNameArray(deliveriesData).reduce((playersNameObjectArray, playerName, i) => {
        const playerNameObj = {};
        playerNameObj.player_id = i + 1;
        playerNameObj.player_name = playerName;
        playersNameObjectArray.push(playerNameObj); 
        return playersNameObjectArray;
    }, [])
}



// For creating JSON data for umpires table using matches.json file.

const CreateJsonDataForUmpires = (matchesData) => {
    return returnUmpiresNameArray(matchesData).reduce((umpiresNameObjectArray, umpireName, i) => {
        const umpireNameObject = {};
        umpireNameObject.umpire_id = i + 1;
        umpireNameObject.umpire_name = umpireName;
        umpiresNameObjectArray.push(umpireNameObject);
        return umpiresNameObjectArray;
    }, [])
} 


const CreateJsonDataForVenue = (matchesData) => {
    const cityVenueObject = returnCityVenueObject(matchesData);
    const cityVenueObjectArray = [];
    let cityId = 1;
    for (let city_venue in cityVenueObject) {
        cityVenueObject[city_venue].forEach(e => {
            const singleCityVenueObject = {};
            singleCityVenueObject.venue_id = cityVenueObjectArray.length + 1;
            singleCityVenueObject.city_id = cityId;
            singleCityVenueObject.city_name = (city_venue !== '')? city_venue: null;
            singleCityVenueObject.venue_name = (e !== '')? e: null;
            cityVenueObjectArray.push(singleCityVenueObject);
        })
        cityId++;
    }
    return cityVenueObjectArray;
}




const CreateJsonDataForMatches = (matchesData, deliveriesData) => {
    const playersNameArray = returnPlayersNameArray(deliveriesData);
    const teamsNameArray = returnTeamsNameArray(matchesData);
    const umpireNameArray = returnUmpiresNameArray(matchesData);
    const cityVenueObject = returnCityVenueObject(matchesData);
        
    return matchesData.reduce((matchesArray, match) => {
        let matchObject = {};
        matchObject = putMatchesDataIntoObject(match, teamsNameArray, playersNameArray, umpireNameArray, cityVenueObject);
        matchesArray.push(matchObject);
        return matchesArray;
    }, [])
}






// For creating JSON data for deliveries table from deliveries.json file.

const CreateJsonDataForDeliveries = (deliveriesData, matchesData) => {
    let last_inning = 0;
    let last_over = 0;
    let over_id = 0;
    const deliveriesArray = deliveriesData.reduce((deliveriesArray, delivery, i) => {
        let deliveries = {};
        if (delivery.inning === last_inning && delivery.over !== last_over) {
            over_id++;
        } else if (delivery.inning !== last_inning && delivery.over !== last_over) {
            over_id++;
        } else if (delivery.inning !== last_inning && delivery.over === last_over) {
            over_id++;
        }

        deliveries = putDeliveriesDataIntoObject(delivery);
        deliveries.delivery_id = i + 1;
        deliveries.over_id = over_id;        

        deliveriesArray.push(deliveries);

        last_inning = delivery.inning;
        last_over = delivery.over;
        return deliveriesArray;
    }, [])

    const playersNameArray = returnPlayersNameArray(deliveriesData);
    const teamsNameArray = returnTeamsNameArray(matchesData);
    return deliveriesArray.reduce((deliveriesArray, delivery) => {
        delivery.batting_team = teamsNameArray.indexOf(delivery.batting_team) + 1;
        delivery.bowling_team = teamsNameArray.indexOf(delivery.bowling_team) + 1;
        delivery.batsman = playersNameArray.indexOf(delivery.batsman) + 1;
        delivery.bowler = playersNameArray.indexOf(delivery.bowler) + 1;
        delivery.non_striker = playersNameArray.indexOf(delivery.non_striker) + 1;
        delivery.player_dismissed = (delivery.player_dismissed !== '')? (playersNameArray.indexOf(delivery.player_dismissed) + 1): null;  
        delivery.dismissal_kind = (delivery.dismissal_kind !== '')? delivery.dismissal_kind: null;  
        delivery.fielder = (delivery.fielder !== '')? (playersNameArray.indexOf(delivery.fielder) + 1): null;  
        deliveriesArray.push(delivery);
        return deliveriesArray;
    }, [])
    
}






const readFileAndReturnArray = (file_path) => {
    return JSON.parse(fs.readFileSync(file_path, "utf-8", (err) => {
        if (err) {
            throw err;
        }
        console.log('data fetched.');
    }));
}


const DumpDataIntoJSONFile = (data, file_name) => {
    fs.writeFile(path.join(__baseDirPath,`data/JSON_data_for_tables/${file_name}.json`), JSON.stringify(data),'utf-8', (err) => {
        if (err) {
            throw err;
        }
        console.log(`${file_name} dumped.`)
    })
} 





/********************
 * Export Statement *
 ********************/

export {
    __baseDirPath,
    __deliveriesJSONFilePath,
    __matchesJSONFilePath,
    // CreateJsonDataForInnings,
    // CreateJsonDataForOvers,
    CreateJsonDataForDeliveries,
    readFileAndReturnArray,
    DumpDataIntoJSONFile,
    CreateJsonDataForTeams,
    CreateJsonDataForPlayers,
    CreateJsonDataForUmpires,
    CreateJsonDataForVenue,
    CreateJsonDataForMatches
}
