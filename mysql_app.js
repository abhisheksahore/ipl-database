/*********************
 * import statements *
 *********************/
import dotenv from 'dotenv';
dotenv.config();

import mysql from 'mysql';
import path from 'path';
import { fileURLToPath } from 'url';
import {readFileAndReturnArray, __deliveriesJSONFilePath, __matchesJSONFilePath} from './ipl_data.js';

const __baseDirPath = path.dirname(fileURLToPath(import.meta.url));
const __deliveriesTableFilePath = path.join(__baseDirPath, `data/JSON_data_for_tables/deliveriesDataForCreatingTable.json`);
const __teamsTableFilePath = path.join(__baseDirPath, `data/JSON_data_for_tables/teamsDataForCreatingTable.json`);
const __playersTableFilePath = path.join(__baseDirPath, `data/JSON_data_for_tables/playersDataForCreatingTable.json`);
const __venuesTableFilePath = path.join(__baseDirPath, `data/JSON_data_for_tables/venuesDataForCreatingTable.json`);
const __umpiresTableFilePath = path.join(__baseDirPath, `data/JSON_data_for_tables/umpiresDataForCreatingTable.json`);
const __matchesTableFilePath = path.join(__baseDirPath, `data/JSON_data_for_tables/matchesDataForCreatingTable.json`);


/*****************************************
 * Creating connection to mysql database *
 *****************************************/

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});


/**************************
 * connecting to database *
 **************************/

connection.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database.');
})


/*******************
 * Creating Tables *
 *******************/


const createTableForGivenQuery = (sqlQuery) => {
    connection.query(sqlQuery, (err) => {
        if (err) throw err;
        console.log('TABLE CREATED.');
    })
}

const sqlQueryForCreatingTableForTeams = `CREATE TABLE teams (
    team_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    team_name VARCHAR(50)
);`
createTableForGivenQuery(sqlQueryForCreatingTableForTeams);

const sqlQueryForCreatingTableForPlayers = `CREATE TABLE players (
    player_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    player_name VARCHAR(50)
);`
createTableForGivenQuery(sqlQueryForCreatingTableForPlayers);




const sqlQueryForCreatingTableForDeliveries = `CREATE TABLE deliveries (
    delivery_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    match_id INT NOT NULL,
    over_id INT NOT NULL,
    inning INT NOT NULL,
    bowling_team int not null, 
    batting_team int not null,
    overs int not null,
    ball INT NOT NULL,
    batsman int NOT NULL,
    non_striker int NOT NULL,
    bowler int not null,
    is_super_over INT NOT NULL,
    wide_runs INT NOT NULL,
    bye_runs INT NOT NULL,
    legbye_runs INT NOT NULL,
    noball_runs INT NOT NULL,
    penalty_runs INT NOT NULL,
    batsman_runs INT NOT NULL,
    extra_runs INT NOT NULL,
    total_runs INT NOT NULL,
    player_dismissed VARCHAR(50),
    dismissal_kind VARCHAR(50),
    fielder VARCHAR(50),

    FOREIGN KEY (batsman) REFERENCES players (player_id),
    FOREIGN KEY (non_striker) REFERENCES players (player_id),
    foreign key (bowling_team) references teams (team_id),
    foreign key (batting_team) references teams (team_id),
    FOREIGN KEY (bowler) REFERENCES players (player_id)
);`
createTableForGivenQuery(sqlQueryForCreatingTableForDeliveries);



const sqlQueryForCreatingTableForUmpires = `CREATE TABLE umpires (
    umpire_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    umpire_name VARCHAR(50)
);`
createTableForGivenQuery(sqlQueryForCreatingTableForUmpires);



const sqlQueryForCreatingTableForVenues = `CREATE TABLE venues (
    venue_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    city_id INT NOT NULL,
    city_name VARCHAR(50),
    venue_name VARCHAR(100)
);`
createTableForGivenQuery(sqlQueryForCreatingTableForVenues);

const sqlQueryForCreatingTableForMatches = `CREATE TABLE matches (
    match_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    venue_id INT NOT NULL,
    season VARCHAR(5),
    date DATE,
    team1 INT NOT NULL,
    team2 INT NOT NULL,
    toss_winner INT,
    toss_decision varchar(20),
    toss_result varchar(20),
    winner int,
    dl_applied int,
    win_by_runs int,
    win_by_wickets int,
    player_of_match int,
    umpire1 int,
    umpire2 int,
    umpire3 int,
    FOREIGN KEY (venue_id) references venues (venue_id),
    FOREIGN KEY (team1) references teams (team_id),
    FOREIGN KEY (team2) references teams (team_id),
    FOREIGN KEY (toss_winner) references teams (team_id),
    FOREIGN KEY (winner) references teams (team_id),
    FOREIGN KEY (player_of_match) references players (player_id),
    FOREIGN KEY (umpire1) references umpires (umpire_id),
    FOREIGN KEY (umpire2) references umpires (umpire_id),
    FOREIGN KEY (umpire3) references umpires (umpire_id)
);`

createTableForGivenQuery(sqlQueryForCreatingTableForMatches);

   


/*****************************
 * Inserting data into table *
 *****************************/

const insertDataIntoTable = (tableName, tableData) => {
    tableData.forEach(e => {
        connection.query(`INSERT INTO ${tableName} SET ?`, e, (err, result) => {
            if (err) throw err;
            console.log('data inserted.');
        })
    })
}

console.log(readFileAndReturnArray(__matchesTableFilePath));

insertDataIntoTable(`teams`, readFileAndReturnArray(__teamsTableFilePath));
insertDataIntoTable(`players`, readFileAndReturnArray(__playersTableFilePath));
insertDataIntoTable(`venues`, readFileAndReturnArray(__venuesTableFilePath));
insertDataIntoTable(`umpires`, readFileAndReturnArray(__umpiresTableFilePath));


insertDataIntoTable(`matches`, readFileAndReturnArray(__matchesTableFilePath));

insertDataIntoTable(`deliveries`, readFileAndReturnArray(__deliveriesTableFilePath));





// Number of matches played per year for all the years in IPL.

connection.query(`select season, count(season) as matches
from matches
group by season;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})




// Number of matches won per team per year in IPL.

connection.query(`select  season, team_name, count(winner) as matches_won
from matches 
join teams on team_id = winner
group by season, winner
order by season;

`, (err, result) => {
    if (err) throw err;
    console.log(result);
})



// Extra runs conceded per team in the year 2016

connection.query(`select team_name, sum(extra_runs) as extra_runs
from deliveries
join matches on matches.match_id = deliveries.match_id
join teams on team_id = bowling_team
where season = '2016'
group by bowling_team;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})




// Top 10 economical bowlers in the year 2015

connection.query(`select player_name, sum(total_runs)/(COUNT(total_runs)/6) as economy
from deliveries
join matches on matches.match_id = deliveries.match_id
join players on player_id = bowler
where season = '2015'
group by bowler
order by economy 
limit 10;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})



//Find the number of times each team won the toss and also won the match

connection.query(`select team_name, count(winner) as number_of_time_won_toss_and_match
from matches
join teams on winner = team_id
where toss_winner = winner
group by winner;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})




// Find the strike rate of a batsman for each season

connection.query(`SELECT season, player_name, 
SUM(batsman_runs)/COUNT(batsman)*100 as strike_rate
FROM deliveries
JOIN matches ON matches.match_id = deliveries.match_id
JOIN players ON player_id = batsman
WHERE player_name LIKE "%v kohli%"
GROUP BY season, batsman;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})




// Find the highest number of times one player has been dismissed by another player.

connection.query(`select p1.player_name as bowler, p2.player_name AS batsman,
count(player_dismissed) as dismissed
from deliveries
join players as p1 on p1.player_id = bowler
join players as p2 on p2.player_id = player_dismissed
where player_dismissed != 'NULL'
group by bowler, player_dismissed
order by dismissed desc
limit 1;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})




// Find the bowler with the best economy in super overs


connection.query(`select player_name, sum(total_runs)/(count(total_runs)/6) as economy
from deliveries
join players on player_id = bowler
where is_super_over = 1
group by bowler
order by economy 
limit 10;
`, (err, result) => {
    if (err) throw err;
    console.log(result);
})


connection.end();















