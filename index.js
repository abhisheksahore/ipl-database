/***********
 * IMPORTS * 
 ***********/  

import {
    __baseDirPath,
    __deliveriesJSONFilePath,
    __matchesJSONFilePath,
    CreateJsonDataForDeliveries,
    readFileAndReturnArray,
    DumpDataIntoJSONFile,
    CreateJsonDataForTeams,
    CreateJsonDataForPlayers,
    CreateJsonDataForUmpires,
    CreateJsonDataForVenue,
    CreateJsonDataForMatches
} from './ipl_data.js';




/*****************
 * Reading Files *
 *****************/

const deliveriesObjectDataArray = readFileAndReturnArray(__deliveriesJSONFilePath); 
const matchesObjectDataArray = readFileAndReturnArray(__matchesJSONFilePath);

console.log(deliveriesObjectDataArray[0]);
console.log(matchesObjectDataArray[0]);




/*********************************
 * Getting data and dumping data *
 *********************************/



const deliveriesDataForCreatingTable = CreateJsonDataForDeliveries(deliveriesObjectDataArray, matchesObjectDataArray); 
DumpDataIntoJSONFile(deliveriesDataForCreatingTable, `deliveriesDataForCreatingTable`);
console.log(deliveriesDataForCreatingTable);


const teamsDataForCreatingTable = CreateJsonDataForTeams(matchesObjectDataArray); 
DumpDataIntoJSONFile(teamsDataForCreatingTable, `teamsDataForCreatingTable`);

const playersDataForCreatingTable = CreateJsonDataForPlayers(deliveriesObjectDataArray); 
DumpDataIntoJSONFile(playersDataForCreatingTable, `playersDataForCreatingTable`);

const umpiresDataForCreatingTable = CreateJsonDataForUmpires(matchesObjectDataArray); 
DumpDataIntoJSONFile(umpiresDataForCreatingTable, `umpiresDataForCreatingTable`);


const venuesDataForCreatingTable = CreateJsonDataForVenue(matchesObjectDataArray); 
DumpDataIntoJSONFile(venuesDataForCreatingTable, `venuesDataForCreatingTable`);


const matchesDataForCreatingTable = CreateJsonDataForMatches(matchesObjectDataArray, deliveriesObjectDataArray); 
DumpDataIntoJSONFile(matchesDataForCreatingTable, `matchesDataForCreatingTable`);
// console.log(matchesDataForCreatingTable);




